helm3 upgrade --install \                      
--kubeconfig ~/.kube/kube-dev.conf \
--set postgresqlPassword=Postgres@12 \     
--set resources.requests.cpu="500m" \
--set resources.requests.memory="512Mi" \
--set resources.limits.cpu="2000m" \
--set persistence.size=20Gi \
--set resources.limits.memory="4Gi" \
--set nodeSelector="Node=Non-Preemptible" \
--set image.tag="12" \
--namespace=infra-services \
demo-postgresql \
bitnami/postgresql
